/*
 * Copyright 2012 Taro L. Saito
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xerial.silk.model

import xerial.silk.util.SilkSpec
import java.io.Reader

//--------------------------------------
//
// SilkIOTest.scala
// Since: 2012/03/14 10:21
//
//--------------------------------------

object SilkIOTest {

  class FASTAParser(in:Reader) {




  }

}

/**
 * @author leo
 */
class SilkIOTest extends SilkSpec {

  "SilkIO" should {

    "support FASTA stream output" in {



    }
  }

}